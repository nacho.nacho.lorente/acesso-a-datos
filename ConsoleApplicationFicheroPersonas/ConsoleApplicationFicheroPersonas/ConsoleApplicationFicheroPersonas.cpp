// ConsoleApplicationFicheroPersonas.cpp: define el punto de entrada de la aplicación de consola.
//


/* Programa en c que gestiones datos de alumno: Nombre(40ca), edad(maximo 10 ca)

1º Cargar datos
2º Escribir datos
3º Insertar tabla
4º Mostrar tabla en pantalla.
5º Borrar todo
6º Salir
*/

#include "stdafx.h"
#include <stdio.h>
#include <string.h>

/* Declaracion de la estructura de alumnos*/

struct TAlumno
{
	char nombre[40];
	int edad;
};
/*===============*/
/*   FUNCIONES   */
/*===============*/

void cargarDatos(
	char nombreFichero[50],
	struct  TAlumno tablaAlumnos[10])
{
	FILE *pf;
	char nombreAlumno[40];
	int edadAlumno,
	posicion;

	posicion = 0;
	pf = fopen(nombreFichero, "rt");
	if (pf != NULL)
	{
		while (!feof(pf)) //feof (file end of file) hace que mientras que sea distinto del final de fichero y haya datos en el fichero continue el bucle
		{
			fscanf(pf, "%s", nombreAlumno); //no se pone & porque el nombre del array ya es un puntero
			fscanf(pf, "%i", &edadAlumno);
			strcpy(tablaAlumnos[posicion].nombre, nombreAlumno); // strcpy para copiar cadenas(arrays)
			tablaAlumnos[posicion].edad = edadAlumno;
			posicion++;
		}
		fclose(pf);
	}
}

void imprimirMenu()
{
	printf("MENU DE OPCIONES \n");
	printf("---------------- \n");
	printf("1. Cargar datos de fichero \n");
	printf("2. Guardar datos en el fichero \n");
	printf("3. Escribir tabla por pantalla \n");
	printf("4. Insertar tabla \n");
	printf("5. Borrar en la tabla\n");
	printf("6. Salir \n");
	printf("Introduce datos: \n");

}


/*=======================*/
/*   PROGRAMA PRINCIPAL  */
/*=======================*/

int main(int argc, char const *argv[])
{
	int opcion;
	struct TAlumno tablaAlumnos[10];

	do
	{
		imprimirMenu();
		opcion = scanf("%i", &opcion);
		switch (opcion)
		{
		case 1: cargarDatos("E:\\2º grado superior\\Acceso a datos\\clase.txt", tablaAlumnos);
			break;
		}
	} while (opcion != 6);

	return 0;
}

//Ayuda de palabra al F1

